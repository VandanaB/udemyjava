package oops;
 
public class Demo {

	public static void main(String[] args) {
		
		//instantiating an object
		Person person1 = new Person(); // new is allocating memory for person1 object
		person1.name = "Peppa";
		person1.phoneNumber = "938438487";
		
		// abstraction: means the class has got many methods but we can use the method we want easily	
		person1.walk();
		person1.number();
		
		 // similarly we can create n number of people by abstracting from class
		Person person2 = new Person();
		person2.name = "george";
		
		person2.walk();
		
	}
}
