package oops;

// Creating class
// class contains variables and methods
public class Person {
	
	// declare variables, attributes, adjectives
	//its like a template
	String name;
	String email;
	String phoneNumber;
	
    //methods are actions, activities, behavior  done by the class
	
	public void walk() {
		System.out.println(name+ " is walking");
	}
	// private methods can be called only inside the same class
	private void eat() {
		System.out.println(name+ " is eating");	
	}
	public void number() {
		System.out.println(name+"'s number is "+ phoneNumber);
		
	}
    
 // private accessModifier
public static void main(String[]args) {
	
	Person n = new Person();
	n.name = "Rakky";
	n.eat();
}
	
}
