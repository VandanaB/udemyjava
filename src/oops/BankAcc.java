package oops;

public class BankAcc  implements IRate {
	
	// Defining Variables
	//instance variables
	String name;
	String accType;
	String accNumber;
	String ssn;
	double deposit=0;	
	double balance=0;
	
	
	//static : belongs to the class not to the object instance
	//final : constant
	static String branch = "IOB keelkattalai";
	static final  String bank = "IOB";
	
	
	//defining constructors : unique methods
	//  1. has same name as the class
	//  2. used to define/ setup/instantiate the object prop
	//  3. Constructors are called IMPLICITLY on INSTANTIATION
	//  4. no returnType
	
	BankAcc(){
		System.out.println("creating new Acc");
	}
	// Overloading: can be defined by changing the argument
	// Overloading is a type of Polymorphism
	BankAcc(String accType){
		System.out.println("New Acc : "+accType);
	}
	
	BankAcc(String accType,double initialDeposit){
		//local variable: declared inside the block (eg. if statement)
		String msg;
		System.out.println(accType + " with balance "+initialDeposit);
		if(initialDeposit<1000) {
			msg = "low balance";
		}else {
			msg = "Thank You!";
	} System.out.println(msg);
      balance=initialDeposit;
	}
	// Defining Methods
	void deposit() {
		System.out.println("Amount deposited is "+ deposit);
	}
	void withdraw() {
		
	}
	
	void balanceCheck() {
		balance= balance+deposit;
		System.out.println("balance: "+balance); 	
	}
	//Type of Polymorphism
	@Override
	public String toString() {
		return"["+ name+ "and"+ accNumber+ "]";
	}
	
	//setters/getters
			public void setName(String name) {
				this.name = "Mr. "+name;
			}
			public String getName() {
				return name;	
			}
		//interface: common class that can be implemented in any unrelated classes
			@Override
			public void setRate() {
				System.out.println("SETRATE");
				
			}
			@Override
			public void Interest() {
				System.out.println("INTEREST");
				
			}
			
	

}
