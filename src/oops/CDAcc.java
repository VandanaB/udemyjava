package oops;


// in Inheritance we can Interface by using implements
public class CDAcc extends BankAcc implements IRate {
	double interestRate;
	void compoundRate() {
		System.out.println(interestRate);
	}
	public static void main (String[]args) {
	BankAcc acc = new BankAcc();
	//gets properties of IRate thats Interface
	acc.setRate();
	// CDAcc can get properties of both CDAcc and BankAcc thats Inheritance
	CDAcc acc1 = new CDAcc();
	acc1.accNumber= "6558e78";
	acc1.compoundRate();
	
	
	  // Polymorphism : based on IRate properties we are pinting BankAcc()
	// we will get only the methods of the Interface 
	IRate samAcc = new BankAcc();
	samAcc.Interest();

	}	

}
