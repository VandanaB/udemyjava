package oops;

public class BankAccApp {

	public static void main(String[] args) {
		// creating object automatically calls the constructor
		BankAcc acc1 = new BankAcc();
		acc1.name = "Rakky";
		acc1.accType = "SB";
		acc1.deposit = 50000;
		acc1.accNumber = "23648467";
		
		//Interface
		
		acc1.setRate();
		acc1.Interest();
		
		// Encapsulation : public API methods
		acc1.setName("Govi");
		System.out.println(acc1.getName());
			
		
		//overriding
		System.out.println(acc1.toString());
		
		
		acc1.deposit();
		
		BankAcc acc2 = new BankAcc("Savings Account");
		acc2.accType = "SB Acc";
		acc2.ssn="7654654";
		
		BankAcc acc3 = new BankAcc("SB Acc", 3000);
		acc3.deposit=1000;
		acc3.balanceCheck();
		// inheritance : extends one class objects into other class
		CDAcc cdAcc = new CDAcc();
		cdAcc.name = "Govi";
		cdAcc.interestRate = 4.5;
		
		
			
		
		
	

	}

}
