package DataStructures;

import java.util.ArrayList;

public class Lists {

	public static void main(String[] args) {
		// 1.create a collection
		ArrayList<String> cities = new ArrayList<String>();
		
		// 2.Add elements of array
		cities.add("Atlanta");
		cities.add("Clevland");
		cities.add("Nashville");
		cities.add("Loiuville");
		System.out.println(cities);
		 //get elements
		for (String city : cities) {
			System.out.println(city);
		}
		
		
		//Get size
		int size = cities.size();
		System.out.println(size);
		
		//retrieve specific element
		System.out.println(cities.get(3));
		
		//remove a specific element
		cities.remove(2);
		System.out.println(cities);
		
		

	}

}
