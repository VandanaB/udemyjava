package DataStructures;

import java.util.HashSet; //prints in random order
import java.util.Set;
import java.util.TreeSet; // alphabetic order



public class Sets {

	public static void main(String[] args) {
		TreeSet<String> animals = new TreeSet<String>();
		 // Create collection
		animals.add("dog");
		animals.add("cow");
		animals.add("tiger");
		animals.add("lion");
		animals.add("fox");
		System.out.println(animals);
		
		animals.add("pig");
		animals.add("bear");
		System.out.println(animals);
		
		// comparing another set
		Set<String> farmAnimals = new HashSet<String>();
		// add elements
		farmAnimals.add("duck");
		farmAnimals.add("cat");
		farmAnimals.add("cow");
		farmAnimals.add("pig");
		
		
		
		Set<String> wildAnimals = new HashSet<String>();
		// add elements
		wildAnimals.add("lion");
		wildAnimals.add("elephant");
		wildAnimals.add("bear");
		wildAnimals.add("tiger");
		
		//intersection animals and farm animals
		Set<String> intersectionSet = new HashSet<String>(animals);
		intersectionSet.retainAll(farmAnimals);
		System.out.println(intersectionSet); 
		//animals and wild animals
		Set<String> intersectionSet1 = new HashSet<String>(animals);
		intersectionSet1.retainAll(wildAnimals);
		System.out.println(intersectionSet1);
		
		
		//union
		Set<String> unionSet = new HashSet<String>(animals);
		unionSet.addAll(farmAnimals);
		System.out.println(unionSet);
		
		//difference
		Set<String> diffSet = new HashSet<String>(farmAnimals);
		diffSet.removeAll(animals);
		System.out.println(diffSet);
		
		
		

	}

}
