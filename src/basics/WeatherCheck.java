package basics;

public class WeatherCheck {
	public static void main(String[]args) {
		int temperature = 85;
		// executes different block based on input
		
		switch(temperature) {
		  case 85: System.out.println("sunny");
		    break; // without break rest of the statement is also executed
		  case 60: System.out.println("cold");
		    break;	
		}
	}
	
}

// for a range go for if else statement if the input is definite go for switch
// if else gives suggestions