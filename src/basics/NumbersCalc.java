package basics;

public class NumbersCalc {
	
	//main method
	public static void main(String[]args) {
		int numA = 10;
		int numB =20;
		addNumbers(numA,numB);     //calling addNumbers method
		//calling another method
		int product = multiplyNum(numA, numB); // here the product is the returnType to the multiplyNum() function  
		int n = numA + product;             // we can use that product for another calculation
		System.out.println(n);
		addNumbers(product, product);
		}
	// The code inside the main is only executed
	
	//Declaring Functions and Methods
	//addNumbers is a function that can add numbers. 
	//we can call this method in the main method
	
	private static void addNumbers(int a, int b) {
		int sum = a+b;
		System.out.println("The sum of " + a+ " and " + b +" is " + sum);	
	}
    static int multiplyNum( int a, int b) {
    	int product = a*b;
    	System.out.println("The product of "+ a +" and "+ b+ " is "+product);
		return product;
		
    }
}
